//
//  main.m
//  UrgentCareWaitTime
//
//  Created by Vijay Gajula on 3/21/14.
//  Copyright (c) 2014 BlueMobileApps. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UrgentCareWaitAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([UrgentCareWaitAppDelegate class]));
    }
}
