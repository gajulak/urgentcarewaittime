//
//  CenterTableCellView.h
//  UrgentCareWaitTime
//
//  Created by Vijay Gajula on 3/22/14.
//  Copyright (c) 2014 BlueMobileApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CenterTableCellView : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *centerName;
@property (weak, nonatomic) IBOutlet UILabel *waitingPeriod;
@property (weak, nonatomic) IBOutlet UILabel *waitingLabel;
@property (weak, nonatomic) IBOutlet UILabel *lastUpdatedLabel;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
@property (weak, nonatomic) IBOutlet UILabel *openHoursLabel;
@property (weak, nonatomic) IBOutlet UILabel *openHoursWeekendLabel;
@property (weak, nonatomic) IBOutlet UIButton *mapButton;

@end
