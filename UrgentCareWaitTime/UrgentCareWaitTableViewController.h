//
//  UrgentCareWaitTableViewController.h
//  UrgentCareWaitTime
//
//  Created by Vijay Gajula on 3/21/14.
//  Copyright (c) 2014 BlueMobileApps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>

@interface UrgentCareWaitTableViewController : UITableViewController <ADBannerViewDelegate>

- (void) loadUrgentCareStatus;
- (void)tableViewDidPullToRefresh;

@property (nonatomic, strong) ADBannerView *bannerForTableFooter;
@property (nonatomic) BOOL bannershouldBeVisible;
@property(nonatomic, assign) BOOL canDisplayBannerAds;
@end
