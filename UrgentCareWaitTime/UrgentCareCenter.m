//
//  UrgentCareCenter.m
//  UrgentCareWaitTime
//
//  Created by Vijay Gajula on 3/21/14.
//  Copyright (c) 2014 BlueMobileApps. All rights reserved.
//

#import "UrgentCareCenter.h"

@implementation UrgentCareCenter

@synthesize name = _name;
@synthesize current_status = _current_status;
@synthesize address = _address;
@synthesize url = _url;
@synthesize lastUpdated = _lastUpdated;
@synthesize openHours = _openHours;
@synthesize openHoursWeekend = _openHoursWeekend;

@end
