//
//  UrgentCareWaitAppDelegate.h
//  UrgentCareWaitTime
//
//  Created by Vijay Gajula on 3/21/14.
//  Copyright (c) 2014 BlueMobileApps. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UrgentCareWaitAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
