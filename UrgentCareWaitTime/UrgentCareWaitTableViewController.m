//
//  UrgentCareWaitTableViewController.m
//  UrgentCareWaitTime
//
//  Created by Vijay Gajula on 3/21/14.
//  Copyright (c) 2014 BlueMobileApps. All rights reserved.
//

#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "UrgentCareWaitTableViewController.h"
#import "UrgentCareCenter.h"
#import "CenterTableCellView.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface UrgentCareWaitTableViewController () {
    NSMutableArray * _objects;
}
@end

@implementation UrgentCareWaitTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"User Agreement"
                                                    message:@"This App should be used for informative purposes only. You should always call and confirm with the medical center before making a decision."
                                                   delegate:nil
                                          cancelButtonTitle:@"Agree"
                                          otherButtonTitles:nil];
    [alert show];
    
    
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor greenColor];
    self.refreshControl = refreshControl;
    [self.refreshControl addTarget:self action:@selector(tableViewDidPullToRefresh) forControlEvents:UIControlEventValueChanged];

    [self.tableView registerNib:[UINib nibWithNibName:@"CenterTableCellView" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"WaitCell"];
   // self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self becomeFirstResponder];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loadUrgentCareStatus) name:UIApplicationWillEnterForegroundNotification object:nil];
    self.bannerForTableFooter.backgroundColor = [UIColor clearColor];
    self.canDisplayBannerAds = YES;
}

-(void) viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self loadUrgentCareStatus];
}

-(void) viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIApplicationWillEnterForegroundNotification object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadUrgentCareStatus {
    NSURL *queryUrl = [NSURL URLWithString:@"http://www.pamf.org/urgent-care/index.php"];
    
    NSData *responseData = [NSData dataWithContentsOfURL:queryUrl];
    NSString* responseString = [[NSString alloc] initWithData:responseData
                                                     encoding:NSASCIIStringEncoding];
    
    // NSLog(@"responseString: %@", responseString);
    
    NSURL *queryUrl2 = [NSURL URLWithString:@"http://www.pamf.org/pediatrics/urgentcare/"];
    NSData *responseData2 = [NSData dataWithContentsOfURL:queryUrl2];
    NSString* responseString2 = [[NSString alloc] initWithData:responseData2
                                                     encoding:NSASCIIStringEncoding];
    responseString2 = [responseString2 stringByReplacingOccurrencesOfString:@"</a>;" withString:@"\""];
    // NSLog(@"responseString2: %@", responseString2);


    NSArray *locations = [NSArray arrayWithObjects:@"Fremont",
                          @"Fremont Pediatric",
                          @"Mountain View",
                          @"Mountain View Pediatric",
                          @"Mountain View Adult",
                          @"Palo Alto Adult",
                          @"Palo Alto Pediatric",
                          @"San Carlos",
                          @"San Mateo",
                          @"Santa Cruz",
                          @"Santa Cruz Main Clinic",
                          @"Santa Cruz Westside",
                          @"Scotts Valley",
                          @"Watsonville",

                          nil];
    
    NSArray *centerAddress = [NSArray arrayWithObjects:@"3200 Kearney Street Level 1, Building 2, Fremont, CA 94538",
                          @"3200 Kearney Street Level 1, Building 2, Fremont, CA 94538",
                          @"701 E. El Camino Real, Second Floor, Mountain View, CA 94040",
                          @"701 E. El Camino Real, Second Floor, Mountain View, CA 94040",
                          @"701 E. El Camino Real, Second Floor, Mountain View, CA 94040",
                          @"795 El Camino Real, Level 1, Lee Building, Palo Alto, CA 94301",
                          @"795 El Camino Real, Level 1, Lee Building, Palo Alto, CA 94301",
                          @"301 Industrial Road, San Carlos, CA 94070",
                          @"100 S. San Mateo Drive, San Mateo, CA 94401",
                          @"2025 Soquel Avenue, Santa Cruz, CA 95062",
                          @"2025 Soquel Avenue, Santa Cruz, CA 95062",
                          @"1203 Mission Street, Santa Cruz, CA 95060",
                          @"4663 Scotts Valley Drive, Scotts Valley, CA 95066",
                          @"550 S. Green Valley Road, Watsonville, CA 95076",
                          
                          nil];

    
    NSArray *phoneNumbers = [NSArray arrayWithObjects:
                             @"510-490-1222", //Fremont
                              @"510-490-1222", //Fremont Pediatric
                              @"650-934-7800", //Mountain View
                              @"650-934-7800", //Mountain View Pediatric
                              @"650-934-7800", //Mountain View Adult
                              @"650-321-4121", //Palo Alto
                              @"650-853-6558", //Palo Alto Pediatric
                              @"650-596-4100", //San Carlos
                              @"650-696-4427", // San Mateo
                              @"831-458-5537", //Santa Cruz
                              @"831-458-5537", //Santa Cruz Main Clinic
                              @"831-458-6310", //Santa Cruz Westside
                              @"831-458-6335", //Scotts Valley
                              @"831-458-5860", //Watsonville
                             nil];
    
    NSArray *openHours = [NSArray arrayWithObjects:
                          @"Monday - Friday, 8 a.m. to 8 p.m.", //Fremont
                             @"Monday - Friday, 8 a.m. to 8 p.m.", //Fremont Pediatric
                             @"Monday - Friday, 8 a.m. to 9 p.m.", //Mountain View
                             @"Monday - Friday, 8 a.m. to 9 p.m.", //Mountain View Pediatric
                             @"Monday - Friday, 8 a.m. to 9 p.m.", //Mountain View Adult
                             @"Monday - Friday, 7 a.m. to 9 p.m.", //Palo Alto
                             @"Monday - Friday, 7 a.m. to 9 p.m.", //Palo Alto Pediatric
                             @"Monday - Friday, 7 a.m. to 9 p.m.", //San Carlos
                             @"Monday - Friday, 9 a.m. to 9 p.m.", //San Mateo
                             @"Monday - Friday, 8 a.m. to 9 p.m.", //Santa Cruz
                             @"Monday - Friday, 8 a.m. to 9 p.m.", //Santa Cruz Main Clinic
                             @"Monday - Friday, 9 a.m. to 9 p.m.", //Santa Cruz Westside
                             @"Monday - Friday, 9 a.m. to 9 p.m.", //Scotts Valley
                             @"Monday - Friday, 9 a.m. to 9 p.m.", //Watsonville
                             nil];

    NSArray *openHoursWeekend = [NSArray arrayWithObjects:
                          @"Saturdays, Sunday & holidays, 8 a.m. to 5 p.m.", //Fremont
                          @"Saturdays, Sunday & holidays, 8 a.m. to 5 p.m.", //Fremont Pediatric
                          @"Saturdays, Sunday & holidays, 8 a.m. to 8 p.m.", //Mountain View
                          @"Saturdays, Sunday & holidays, 8 a.m. to 8 p.m.", //Mountain View Pediatric
                          @"Saturdays, Sunday & holidays, 8 a.m. to 8 p.m.", //Mountain View Adult
                          @"Saturdays, Sunday & holidays, 7 a.m. to 9 p.m.", //Palo Alto
                          @"Saturdays, Sunday & holidays, 8 a.m. to 9 p.m.", //Palo Alto Pediatric
                          @"Saturdays, Sunday & holidays, 7 a.m. to 9 p.m.", //San Carlos
                          @"Saturdays, Sunday & Holidays, 9 a.m. to 5 p.m",  //San Mateo
                          @"Saturdays, Sunday & holidays, 8 a.m. to 9 p.m.", //Santa Cruz
                          @"Saturdays, Sunday & holidays, 8 a.m. to 9 p.m.", //Santa Cruz Main Clinic
                          @"Saturdays, Sunday & holidays, 9 a.m. to 6 p.m.", //Santa Cruz Westside
                          @"Saturdays, Sunday & holidays, 9 a.m. to 6 p.m.", //Scotts Valley
                          @"Saturdays, Sunday & holidays, 9 a.m. to 6 p.m.", //Watsonville
                          nil];
    
   

    NSMutableArray *newCenters = [[NSMutableArray alloc] initWithCapacity:0];
    UrgentCareCenter *center;
    BOOL unknown;
    for (NSString *loc in locations) {
        NSString *center_string = [NSString stringWithFormat:@"<strong>%@ Urgent Care:</strong>", loc];
        unknown = false;
        NSRange r1 = [responseString rangeOfString:center_string];
        if (r1.location == NSNotFound) {
            r1 = [responseString2 rangeOfString:center_string];
        }
        
        if (r1.length > 1) {
            NSString *end = [responseString substringFromIndex:r1.location + r1.length];
        
            NSRange r2 = [end rangeOfString:@".</div>"];
            if (r2.length < 1 ) {
                unknown = true;
                goto unknown_state;
            }
            
            NSRange rSub = NSMakeRange(r1.location + r1.length, r2.location);
            if (rSub.length < 1) {
                unknown = true;
                goto unknown_state;
            }
        
            NSString *sub = [responseString substringWithRange:rSub];
        
            if (sub == nil) {
                unknown = true;
                goto unknown_state;
            }
            
           // NSLog(@"sub: %@", sub);
            center = [[UrgentCareCenter alloc] init];
            [newCenters addObject:center];
            center.name = [NSString stringWithFormat:@" %@ Urgent Care",loc];
            
            NSRange r3 = [sub rangeOfString:@"As of "];
            if (r3.length > 1) {
                NSString *r3end = [sub substringFromIndex:r3.location + r3.length];
                NSRange r4 = [r3end rangeOfString:@".,"];
                if (r4.length > 1) {
                    NSRange r3Sub = NSMakeRange(r3.location + r3.length, r4.location);
                    if (r3Sub.length > 1) {
                        center.lastUpdated = [NSString stringWithFormat:@"Updated: %@",[sub substringWithRange:r3Sub]];
                    }
                    
                    NSRange r5 = [sub rangeOfString:@"the wait time is "];
                    if (r5.length > 1) {
                        center.current_status = [NSString stringWithFormat:@"Waiting: %@",[sub substringFromIndex:r5.location + r5.length]];
                    }
                }
            }
            
            center.phone_number = [phoneNumbers objectAtIndex:[locations indexOfObject:loc]];
            center.openHours = [NSString stringWithFormat:@"   %@",[openHours objectAtIndex:[locations indexOfObject:loc]]];
            center.openHoursWeekend = [NSString stringWithFormat:@"      %@",[openHoursWeekend objectAtIndex:[locations indexOfObject:loc]]];
            center.address = [centerAddress objectAtIndex:[locations indexOfObject:loc]];
        } else {
            center_string = [NSString stringWithFormat:@"%@ Urgent Care(.)*?is now closed", loc];
            r1 = [responseString rangeOfString:center_string options:NSRegularExpressionSearch];
            if (r1.location == NSNotFound) {
                r1 = [responseString2 rangeOfString:center_string options:NSRegularExpressionSearch];
            }
            if (r1.length > 1) {
                center = [[UrgentCareCenter alloc] init];
                [newCenters addObject:center];
                center.name = [NSString stringWithFormat:@" %@ Urgent Care",loc];
                center.current_status = @"is now closed";
                center.lastUpdated = @"";
                center.phone_number = [phoneNumbers objectAtIndex:[locations indexOfObject:loc]];
                center.openHours = [NSString stringWithFormat:@"   %@",[openHours objectAtIndex:[locations indexOfObject:loc]]];
                center.openHoursWeekend = [NSString stringWithFormat:@"      %@",[openHoursWeekend objectAtIndex:[locations indexOfObject:loc]]];
                center.address = [centerAddress objectAtIndex:[locations indexOfObject:loc]];
            } else {
                unknown = true;
                goto unknown_state;
            }
        }
    unknown_state:
        if (unknown == true) {
            center = [[UrgentCareCenter alloc] init];
            [newCenters addObject:center];
            center.name = [NSString stringWithFormat:@" %@ Urgent Care",loc];
            center.current_status = @"unknown";
            center.lastUpdated = @"";
            center.phone_number = [phoneNumbers objectAtIndex:[locations indexOfObject:loc]];
            center.openHours = [NSString stringWithFormat:@"   %@",[openHours objectAtIndex:[locations indexOfObject:loc]]];
            center.openHoursWeekend = [NSString stringWithFormat:@"      %@",[openHoursWeekend objectAtIndex:[locations indexOfObject:loc]]];
            center.address = [centerAddress objectAtIndex:[locations indexOfObject:loc]];
        }
    }
    
    _objects = newCenters;
    [self.tableView reloadData];
    [self.refreshControl endRefreshing];
}

- (void)tableViewDidPullToRefresh
{
    [_objects removeAllObjects];
    [self loadUrgentCareStatus];
}

-(void)phoneCallButtonClicked: (UIControl *) button withEvent: (UIEvent *) event
{
    CGPoint buttonPosition = [button convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        UrgentCareCenter *thisCenter = [_objects objectAtIndex:indexPath.row];
        NSString *phoneURL = [NSString stringWithFormat:@"telprompt:%@", thisCenter.phone_number];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneURL]];
    }
}
    
-(void)mapButtonClicked: (UIControl *) button withEvent: (UIEvent *) event
{
    CGPoint buttonPosition = [button convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        UrgentCareCenter *thisCenter = [_objects objectAtIndex:indexPath.row];
        Class mapItemClass = [MKMapItem class];
        if (mapItemClass && [mapItemClass respondsToSelector:@selector(openMapsWithItems:launchOptions:)])
        {
            CLGeocoder *geocoder = [[CLGeocoder alloc] init];
            [geocoder geocodeAddressString:thisCenter.address
                         completionHandler:^(NSArray *placemarks, NSError *error) {
                             
                             // Convert the CLPlacemark to an MKPlacemark
                             // Note: There's no error checking for a failed geocode
                             CLPlacemark *geocodedPlacemark = [placemarks objectAtIndex:0];
                             MKPlacemark *placemark = [[MKPlacemark alloc]
                                                       initWithCoordinate:geocodedPlacemark.location.coordinate
                                                       addressDictionary:geocodedPlacemark.addressDictionary];
                             
                             // Create a map item for the geocoded address to pass to Maps app
                             MKMapItem *mapItem = [[MKMapItem alloc] initWithPlacemark:placemark];
                             [mapItem setName:geocodedPlacemark.name];
                             
                             // Set the directions mode to "Driving"
                             // Can use MKLaunchOptionsDirectionsModeWalking instead
                             NSDictionary *launchOptions = @{MKLaunchOptionsDirectionsModeKey : MKLaunchOptionsDirectionsModeDriving};
                             
                             // Get the "Current User Location" MKMapItem
                             MKMapItem *currentLocationMapItem = [MKMapItem mapItemForCurrentLocation];
                             
                             // Pass the current location and destination map items to the Maps app
                             // Set the direction mode in the launchOptions dictionary
                             [MKMapItem openMapsWithItems:@[currentLocationMapItem, mapItem] launchOptions:launchOptions];
                             
                         }];
        }
    }
}

#pragma mark - Table view data source
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel *lbl = [[UILabel alloc] init];
    lbl.textAlignment = NSTextAlignmentCenter;
    lbl.font = [UIFont fontWithName:@"Avenir Next Condensed Demi Bold" size:18.0];
    lbl.textColor = [UIColor darkTextColor];
    lbl.backgroundColor = [UIColor whiteColor];
    lbl.text = @"Medical Centers";
    return lbl;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 32;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _objects.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CenterTableCellView *cell = [tableView dequeueReusableCellWithIdentifier:@"WaitCell"];
    if (cell == nil) {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"CenterTableCellView" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    

    UrgentCareCenter *thisCenter = [_objects objectAtIndex:indexPath.row];
    
    cell.centerName.text = thisCenter.name;
    cell.waitingPeriod.text = thisCenter.current_status;
    cell.lastUpdatedLabel.text = thisCenter.lastUpdated;
    cell.openHoursLabel.text = thisCenter.openHours;
    cell.openHoursWeekendLabel.text = thisCenter.openHoursWeekend;
    
    [cell.callButton addTarget:self
                        action:@selector(phoneCallButtonClicked:withEvent:)
              forControlEvents:UIControlEventTouchUpInside];
    [cell.mapButton addTarget:self
                        action:@selector(mapButtonClicked:withEvent:)
              forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

#pragma iAd Delegate Methods

-(ADBannerView *) bannerForTableFooter {
    if (!_bannerForTableFooter) {
        _bannerForTableFooter = [[ADBannerView alloc] initWithAdType:ADAdTypeBanner];
        _bannerForTableFooter.delegate = self;
    }
    return _bannerForTableFooter;
}

-(void) bannerViewDidLoadAd:(ADBannerView *)banner {
   // self.bannershouldBeVisible = YES;
   // [self.tableView reloadData];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    [banner setAlpha:1];
    [UIView commitAnimations];
}

-(void) bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error {
   // self.bannershouldBeVisible = NO;
   // [self.tableView reloadData];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:1];
    [banner setAlpha:0];
    [UIView commitAnimations];
}
@end
