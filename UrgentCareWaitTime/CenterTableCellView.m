//
//  CenterTableCellView.m
//  UrgentCareWaitTime
//
//  Created by Vijay Gajula on 3/22/14.
//  Copyright (c) 2014 BlueMobileApps. All rights reserved.
//

#import "CenterTableCellView.h"

@implementation CenterTableCellView

@synthesize centerName;
@synthesize waitingPeriod;
@synthesize waitingLabel;
@synthesize lastUpdatedLabel;
@synthesize callButton;
@synthesize mapButton;
@synthesize openHoursLabel;
@synthesize openHoursWeekendLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
@end
