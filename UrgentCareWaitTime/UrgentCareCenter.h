//
//  UrgentCareCenter.h
//  UrgentCareWaitTime
//
//  Created by Vijay Gajula on 3/21/14.
//  Copyright (c) 2014 BlueMobileApps. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UrgentCareCenter : NSObject

@property(nonatomic, copy) NSString *name;
@property(nonatomic, copy) NSString *current_status;
@property(nonatomic, copy) NSString *phone_number;
@property(nonatomic, copy) NSString *address;
@property(nonatomic, copy) NSString *url;
@property(nonatomic, copy) NSString *lastUpdated;
@property(nonatomic, copy) NSString *openHours;
@property(nonatomic, copy) NSString *openHoursWeekend;

@end
